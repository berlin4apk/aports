# Maintainer: Josef Vybíhal <josef.vybihal@gmail.com>
pkgname=radsecproxy
pkgver=1.9.1
pkgrel=0
pkgdesc="Generic RADIUS proxy that supports both UDP and TLS (RadSec) RADIUS transports"
url="https://github.com/radsecproxy/radsecproxy"
license="BSD-3-Clause"
arch="all"
makedepends="nettle-dev openssl-dev autoconf automake"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://github.com/radsecproxy/radsecproxy/releases/download/$pkgver/radsecproxy-$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	"

prepare() {
	default_prepare
	autoreconf -fi
}

build() {
	./configure \
		--prefix=/usr \
		--bindir=/usr/bin \
		--sbindir=/usr/bin \
		--sysconfdir=/etc/radsecproxy
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm644 radsecproxy.conf-example "$pkgdir"/etc/radsecproxy/radsecproxy.conf

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
e72a3b55089cb985503c74a0aa0c62e0a05ac5325217900d39aa87563dd639f154ee372ac5e62ecd5aed92b832b0e58bc43f9588ed7950ec731fa5b297e5c07c  radsecproxy-1.9.1.tar.gz
fb9b2761071f3773fe4c15ed74372333c0d7f39feb84eb48dbf8b531c85a5d4d3ae15fe8933f3f56c57bbf5047e9e08b652f0c44d7e974ec8d56909f3e308f30  radsecproxy.initd
e1e7b62fd92d7433430cbef5fcb99b0213f140c8aaa03f63ac2f432cead005e276c34ce2f8f6473b602e85160dc3cf8cb2e5209a6f026b740b4056208b40d073  radsecproxy.confd
"
