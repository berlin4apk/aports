From 9b3ebef85d4c5d13bbbc0f12f80492eb0ed16960 Mon Sep 17 00:00:00 2001
From: Christian Brauner <christian.brauner@ubuntu.com>
Date: Wed, 6 Oct 2021 10:56:12 +0200
Subject: [PATCH] lxd: support core scheduling for virtual machines

Add a new forkcoresched command. We fork off a new helper process that
moves itself into a new core scheduling domain and then bestows that
core scheduling domain onto the vcpu threads. This works around a
missing feature in the current kernel api.

Signed-off-by: Christian Brauner <christian.brauner@ubuntu.com>
---
 lxd/include/syscall_wrappers.h        | 18 +++++
 lxd/instance/drivers/driver_common.go | 18 +++++
 lxd/instance/drivers/driver_qemu.go   | 21 ++++--
 lxd/main.go                           |  4 ++
 lxd/main_forkcoresched.go             | 97 +++++++++++++++++++++++++++
 lxd/main_nsexec.go                    |  3 +
 6 files changed, 154 insertions(+), 7 deletions(-)
 create mode 100644 lxd/main_forkcoresched.go

diff --git a/lxd/include/syscall_wrappers.h b/lxd/include/syscall_wrappers.h
index 4832b6637..29a3373a9 100644
--- a/lxd/include/syscall_wrappers.h
+++ b/lxd/include/syscall_wrappers.h
@@ -121,10 +121,28 @@ static inline int core_scheduling_cookie_create_threadgroup(pid_t pid)
 	return 0;
 }
 
+static inline int core_scheduling_cookie_create_thread(pid_t pid)
+{
+	int ret;
+
+	ret = prctl(PR_SCHED_CORE, PR_SCHED_CORE_CREATE, pid,
+		    PR_SCHED_CORE_SCOPE_THREAD, 0);
+	if (ret)
+		return -errno;
+
+	return 0;
+}
+
 static inline int core_scheduling_cookie_share_with(pid_t pid)
 {
 	return prctl(PR_SCHED_CORE, PR_SCHED_CORE_SHARE_FROM, pid,
 		     PR_SCHED_CORE_SCOPE_THREAD, 0);
 }
 
+static inline int core_scheduling_cookie_share_to(pid_t pid)
+{
+	return prctl(PR_SCHED_CORE, PR_SCHED_CORE_SHARE_TO, pid,
+		     PR_SCHED_CORE_SCOPE_THREAD, 0);
+}
+
 #endif /* __LXD_SYSCALL_WRAPPER_H */
diff --git a/lxd/instance/drivers/driver_common.go b/lxd/instance/drivers/driver_common.go
index db7c86f61..13e79a49e 100644
--- a/lxd/instance/drivers/driver_common.go
+++ b/lxd/instance/drivers/driver_common.go
@@ -4,6 +4,7 @@ import (
 	"database/sql"
 	"fmt"
 	"path/filepath"
+	"strconv"
 	"strings"
 	"time"
 
@@ -962,3 +963,20 @@ func (d *common) recordLastState() error {
 		return nil
 	})
 }
+
+func (d *common) setCoreSched(pids []int) error {
+	if !d.state.OS.CoreScheduling {
+		return nil
+	}
+
+	args := []string{
+		"forkcoresched",
+	}
+
+	for _, pid := range pids {
+		args = append(args, strconv.Itoa(pid))
+	}
+
+	_, err := shared.RunCommand(d.state.OS.ExecPath, args...)
+	return err
+}
diff --git a/lxd/instance/drivers/driver_qemu.go b/lxd/instance/drivers/driver_qemu.go
index 03550ef11..e894e3a42 100644
--- a/lxd/instance/drivers/driver_qemu.go
+++ b/lxd/instance/drivers/driver_qemu.go
@@ -1371,6 +1371,20 @@ func (d *qemu) Start(stateful bool) error {
 		return err
 	}
 
+	// Get the list of PIDs from the VM.
+	pids, err := monitor.GetCPUs()
+	if err != nil {
+		op.Done(err)
+		return err
+	}
+
+	err = d.setCoreSched(pids)
+	if err != nil {
+		err = fmt.Errorf("Failed to allocate new core scheduling domain for vCPU threads: %w", err)
+		op.Done(err)
+		return err
+	}
+
 	// Apply CPU pinning.
 	cpuLimit, ok := d.expandedConfig["limits.cpu"]
 	if ok && cpuLimit != "" {
@@ -1383,13 +1397,6 @@ func (d *qemu) Start(stateful bool) error {
 				return err
 			}
 
-			// Get the list of PIDs from the VM.
-			pids, err := monitor.GetCPUs()
-			if err != nil {
-				op.Done(err)
-				return err
-			}
-
 			// Confirm nothing weird is going on.
 			if len(pins) != len(pids) {
 				err = fmt.Errorf("QEMU has less vCPUs than configured")
diff --git a/lxd/main.go b/lxd/main.go
index 91976e9f9..3e4dd1621 100644
--- a/lxd/main.go
+++ b/lxd/main.go
@@ -146,6 +146,10 @@ func main() {
 	forksyscallCmd := cmdForksyscall{global: &globalCmd}
 	app.AddCommand(forksyscallCmd.Command())
 
+	// forkcoresched sub-command
+	forkcoreschedCmd := cmdForkcoresched{global: &globalCmd}
+	app.AddCommand(forkcoreschedCmd.Command())
+
 	// forkmount sub-command
 	forkmountCmd := cmdForkmount{global: &globalCmd}
 	app.AddCommand(forkmountCmd.Command())
diff --git a/lxd/main_forkcoresched.go b/lxd/main_forkcoresched.go
new file mode 100644
index 000000000..ac2437056
--- /dev/null
+++ b/lxd/main_forkcoresched.go
@@ -0,0 +1,97 @@
+package main
+
+import (
+	"fmt"
+
+	"github.com/spf13/cobra"
+
+	// Used by cgo
+	_ "github.com/lxc/lxd/lxd/include"
+)
+
+/*
+#ifndef _GNU_SOURCE
+#define _GNU_SOURCE 1
+#endif
+#include <fcntl.h>
+#include <libgen.h>
+#include <sched.h>
+#include <stdbool.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <string.h>
+#include <sys/prctl.h>
+#include <sys/types.h>
+#include <unistd.h>
+
+#include "include/memory_utils.h"
+#include "include/mount_utils.h"
+#include "include/syscall_numbers.h"
+#include "include/syscall_wrappers.h"
+
+extern char* advance_arg(bool required);
+
+void forkcoresched(void)
+{
+	char *cur = NULL;
+	int ret;
+	__u64 cookie;
+
+	// Check that we're root
+	if (geteuid() != 0)
+		_exit(EXIT_FAILURE);
+
+	// Get the subcommand
+	cur = advance_arg(false);
+	if (cur == NULL ||
+	    (strcmp(cur, "--help") == 0 ||
+	     strcmp(cur, "--version") == 0 || strcmp(cur, "-h") == 0))
+		_exit(EXIT_SUCCESS);
+
+	ret = core_scheduling_cookie_create_thread(0);
+	if (ret)
+		_exit(EXIT_FAILURE);
+
+	cookie = core_scheduling_cookie_get(0);
+	if (!core_scheduling_cookie_valid(cookie))
+		_exit(EXIT_FAILURE);
+
+	for (const char *pidstr = cur; pidstr; pidstr = advance_arg(false)) {
+		ret = core_scheduling_cookie_share_to(atoi(pidstr));
+		if (ret)
+			_exit(EXIT_FAILURE);
+
+		cookie = core_scheduling_cookie_get(0);
+		if (!core_scheduling_cookie_valid(cookie))
+			_exit(EXIT_FAILURE);
+	}
+
+	_exit(EXIT_SUCCESS);
+}
+*/
+import "C"
+
+type cmdForkcoresched struct {
+	global *cmdGlobal
+}
+
+func (c *cmdForkcoresched) Command() *cobra.Command {
+	// Main subcommand
+	cmd := &cobra.Command{}
+	cmd.Use = "forkcoresched <PID> [...]"
+	cmd.Short = "Create new core scheduling domain"
+	cmd.Long = `Description:
+  Create new core scheduling domain
+
+  This command is used to move a set of processes into a new core scheduling
+  domain.
+`
+	cmd.RunE = c.Run
+	cmd.Hidden = true
+
+	return cmd
+}
+
+func (c *cmdForkcoresched) Run(cmd *cobra.Command, args []string) error {
+	return fmt.Errorf("This command should have been intercepted in cgo")
+}
diff --git a/lxd/main_nsexec.go b/lxd/main_nsexec.go
index e35bf7412..78e13256d 100644
--- a/lxd/main_nsexec.go
+++ b/lxd/main_nsexec.go
@@ -49,6 +49,7 @@ import (
 
 // External functions
 extern void checkfeature();
+extern void forkcoresched();
 extern void forkexec();
 extern void forkfile();
 extern void forksyscall();
@@ -370,6 +371,8 @@ __attribute__((constructor)) void init(void) {
 		forkproxy();
 	else if (strcmp(cmdline_cur, "forkuevent") == 0)
 		forkuevent();
+	else if (strcmp(cmdline_cur, "forkcoresched") == 0)
+		forkcoresched();
 	else if (strcmp(cmdline_cur, "forkzfs") == 0) {
 		ret = unshare(CLONE_NEWNS);
 		if (ret < 0) {
-- 
2.33.1

