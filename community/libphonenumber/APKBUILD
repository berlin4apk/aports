# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libphonenumber
pkgver=8.12.35
pkgrel=0
pkgdesc="Library for parsing, formatting, and validating international phone numbers."
url="https://github.com/googlei18n/libphonenumber"
arch="all"
license="Apache-2.0"
depends_dev="
	boost-dev
	icu-dev
	protobuf-dev
	"
makedepends="$depends_dev
	cmake
	gtest-dev
	ninja
	"
checkdepends="gtest"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/googlei18n/libphonenumber/archive/v$pkgver/libphonenumber-v$pkgver.tar.gz
	cmake-duplicate-rule-definition.patch
	"

build() {
	cd cpp
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DREGENERATE_METADATA=OFF
	cmake --build build
}

check() {
	cd cpp
	./build/libphonenumber_test
}

package() {
	cd cpp
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1992a9fc3e46749311d19d2b67e232de9b819d96baaed1ba618714969b06417a15181b80c5461e41ad49cdc0ed6bcb3f2ef93c87b8e22caa1bfeea5e6f8a200f  libphonenumber-v8.12.35.tar.gz
dc76f0649c401ec97a7449373b96247135c3b80cf1e5bb8afba005fed1055a74429d3c778b7519b609bdcfbb278be395ef83e0bd00228239b6f15d96b0d2df11  cmake-duplicate-rule-definition.patch
"
