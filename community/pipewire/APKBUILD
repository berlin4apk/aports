# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pipewire
pkgver=0.3.39
pkgrel=3
pkgdesc="Multimedia processing graphs"
url="https://pipewire.org/"
# s390x blocked due to failing tests on big-endian
arch="all !s390x"
license="LGPL-2.1-or-later"
# The launch script /usr/libexec/pipewire-launcher uses an argument that is unsupported by Busybox's pkill
# So we use pkill provided by procps instead
depends="procps"
makedepends="
	alsa-lib-dev
	avahi-dev
	bash
	bluez-dev
	dbus-dev
	doxygen
	eudev-dev
	fdk-aac-dev
	glib-dev
	graphviz
	gst-plugins-base-dev
	gstreamer-dev
	jack-dev
	libfreeaptx-dev
	libldac-dev
	libusb-dev
	libx11-dev
	meson
	ncurses-dev
	pulseaudio-dev
	py3-docutils
	sbc-dev
	vulkan-loader-dev
	xmltoman
	"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-libs
	$pkgname-alsa
	$pkgname-pulse
	$pkgname-jack
	gst-plugin-pipewire:gst_plugin
	$pkgname-zeroconf
	$pkgname-spa-bluez
	$pkgname-spa-vulkan
	$pkgname-tools
	$pkgname-spa-tools:spa_tools
	$pkgname-lang
	"
install="$pkgname.post-upgrade"
source="https://gitlab.freedesktop.org/PipeWire/pipewire/-/archive/$pkgver/pipewire-$pkgver.tar.gz
	0001-fix-intl-lookup.patch
	0002-cpu-fix-compilation-on-some-architectures.patch
	0003-test-fix-tmpdir-value-after-setenv.patch
	pipewire.desktop
	pipewire-launcher.sh
	"

case "$CARCH" in
	# Limited by webrtc-audio-processing-dev
	x86 | x86_64 | aarch64)
		makedepends="$makedepends webrtc-audio-processing-dev"
		subpackages="$subpackages $pkgname-echo-cancel:echo_cancel"
	;;
esac

build() {
	abuild-meson \
		-Ddocs=disabled \
		-Dman=enabled \
		-Dgstreamer=enabled \
		-Dexamples=enabled \
		-Dffmpeg=disabled \
		-Dsystemd=disabled \
		-Dvulkan=enabled \
		-Dsdl2=disabled \
		-Dsession-managers=[] \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	install -Dm644 "$srcdir"/pipewire.desktop -t "$pkgdir"/etc/xdg/autostart/
	install -Dm755 "$srcdir"/pipewire-launcher.sh "$pkgdir"/usr/libexec/pipewire-launcher
}

alsa() {
	pkgdesc="ALSA support for pipewire"
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/alsa-lib
	amove usr/share/alsa/alsa.conf.d

	cd "$subpkgdir"

	mkdir -p etc/alsa/conf.d
	cp usr/share/alsa/alsa.conf.d/* etc/alsa/conf.d
}

pulse() {
	pkgdesc="Pulseaudio support for pipewire"
	depends="pipewire-session-manager"
	provides="pulseaudio=$pkgver-r$pkgrel pulseaudio-bluez=$pkgver-r$pkgrel pulseaudio-alsa=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/bin/pipewire-pulse
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-protocol-pulse.so
	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-pulse-tunnel.so
	amove usr/share/pipewire/pipewire-pulse.conf

}

jack() {
	pkgdesc="JACK support for pipewire"
	depends="pipewire-session-manager"

	amove usr/lib/pipewire-*/jack
	amove usr/bin/pw-jack
	amove usr/lib/spa-*/jack/libspa-jack.so
	amove usr/share/pipewire/jack.conf
}

gst_plugin() {
	pkgdesc="Multimedia graph framework - PipeWire plugin"
	depends="pipewire-session-manager gst-plugins-base"

	amove usr/lib/gstreamer-1.0
}

echo_cancel() {
	pkgdesc="WebRTC-based echo canceller module for PipeWire"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-echo-cancel.so
}

zeroconf() {
	pkgdesc="$pkgdesc - Zeroconf support"
	depends=""
	provides="pulseaudio-zeroconf=$pkgver-r$pkgrel"
	provider_priority=1

	amove usr/lib/pipewire-${pkgver%.*}/libpipewire-module-zeroconf-discover.so
}

bluez() {
	pkgdesc="PipeWire BlueZ5 SPA plugin (Bluetooth)"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/lib/spa-*/bluez5
}

vulkan() {
	pkgdesc="PipeWire Vulkan SPA plugin"
	depends=""

	amove usr/lib/spa-*/vulkan
}

tools() {
	pkgdesc="PipeWire tools"
	depends="$pkgname=$pkgver-r$pkgrel"
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/pw-*
}

spa_tools() {
	pkgdesc="PipeWire SPA tools"
	depends=""
	replaces="$pkgname"  # for backward compatibility

	amove usr/bin/spa-*
}

sha512sums="
75ed35177577efe7559341394605777be21d0f3952b9379a23c8d4c71d35ed88ec45c3e9ad58832099c8f370753d8b47a5767f47453a599c4f4a4b641b2b0d9d  pipewire-0.3.39.tar.gz
04f7420cb9aedde53f9d51061bbe010913690934630bc5f1ee8d1c3f6a9fd66ac2c2660f2410d63f69a16c6cba17d1764c9183a0aec8ccd3cef0a035aa9e4f45  0001-fix-intl-lookup.patch
cd230f80b01c4d07225ded8f9c5bab7f3d063e76f2c49956b9d8a08dcace291f5625a33268631b432b3f24f8bdbddb0da42c4418ed06c5840e1c11b1d8204a80  0002-cpu-fix-compilation-on-some-architectures.patch
18e7ff1527ce49311d82b2816c22fcccd3c0fd89cee4ae617b606b9f7b73c1833e67b28e867ea7e772899857b0ac06ef9c91fcc253b720e8a42ff17fb963b69e  0003-test-fix-tmpdir-value-after-setenv.patch
d5d8bc64e42715aa94296e3e26e740142bff7f638c7eb4fecc0301e46d55636d889bdc0c0399c1eb523271b20f7c48cc03f6ce3c072e0e8576c821ed1ea0e3dd  pipewire.desktop
51a1733571a09f73f30a4e98424dfdf96b2f73036843738f9579bb3fff08019d9136ee0e434124e46b6e1066a7f7aa1cd9580aedd7ec17573031be435ae1201b  pipewire-launcher.sh
"
