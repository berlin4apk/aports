# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-systemmonitor
pkgver=5.23.2
pkgrel=0
pkgdesc="An application for monitoring system resources"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	knewstuff-dev
	kservice-dev
	libksysguard-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-systemmonitor-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ksystemstatstest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
314ead3460cba7f9f1ef72e5b618e9361d270fda7f3e1d79dd1ab4ac2417c3a81e54924e7d54ff660edf969eb4fd929a1f03b437e7d0f7287d9c4ff6f88fdb54  plasma-systemmonitor-5.23.2.tar.xz
"
