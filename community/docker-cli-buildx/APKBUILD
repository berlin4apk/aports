# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-buildx
_commit=266c0eac611d64fcc0c72d80206aa364e826758d
pkgver=0.6.3
pkgrel=3
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/engine/reference/commandline/buildx_build"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="buildx-$pkgver.tar.gz::https://github.com/docker/buildx/archive/v$pkgver.tar.gz"

_buildx_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/buildx-"$pkgver"

export GOPATH=$srcdir/go
export GOCACHE=$srcdir/go-build
export GOTMPDIR=$srcdir

build() {
	PKG=github.com/docker/buildx
	local ldflags="-X $PKG/version.Version=v$pkgver -X $PKG/version.Revision=$_commit -X $PKG/version.Package=$PKG"
	go build -modcacherw -ldflags "$ldflags" -o docker-buildx ./cmd/buildx
}

check() {
	# filecount tests are excluded because they depend on exact file sizes
	# which depend on filesystem used.
	local pkgs="$(go list -modcacherw ./... | grep -Ev '(plugins/inputs/filecount)')"
	go test -modcacherw -short $pkgs
	./docker-buildx version
}

package() {
	install -Dm755 docker-buildx "$pkgdir$_buildx_installdir"/docker-buildx
}

sha512sums="
ee088d6f1e8db8eba109a6869e3f125f99aea7a2741a9a89bc21d46491ef454947af2d51c7e5d4ab4134775b8018e4bb18336cc3da3520e96f2a5939100625ed  buildx-0.6.3.tar.gz
"
