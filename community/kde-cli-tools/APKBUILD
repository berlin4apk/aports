# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-cli-tools
pkgver=5.23.2
pkgrel=0
pkgdesc="Tools based on KDE Frameworks 5 to better interact with the system"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://invent.kde.org/plasma/kde-cli-tools"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0-or-later AND GPL-2.0-only AND LGPL-2.1-only"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	kdesu-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kservice-dev
	kwindowsystem-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

# Workaround a circular dependency https://gitlab.alpinelinux.org/alpine/aports/-/issues/11785
install_if="plasma-workspace"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8b49ae342c270da943a5a41520996aea06df5593c82f4706505b01771b78c5dbaa44fe996298098d95f78a851382e088e2c48a276f7562e9bac7a25a91b09ca8  kde-cli-tools-5.23.2.tar.xz
"
